class redis(
  Variant[String, Boolean] $ensure = present,
  Variant[String, Boolean] $service_ensure = running,
  Optional[Hash] $settings = {},
) inherits redis::params {
  notice('---')

  $app_settings = $redis::params::settings + std::nv($settings, Hash, {})
  info(std::pp({'app_settings' => $app_settings}))

  $file_ensure = std::file_ensure($ensure)
  $dir_ensure = std::dir_ensure($ensure)

  contain redis::install
  contain redis::config
  contain redis::service

  case $ensure {
    'absent', false: {
      # absent の場合は disable service を先に行う
      Class[redis::service]
      -> Class[redis::install]
      -> Class[redis::config]
    }
    default: {
      Class[redis::install]
      -> Class[redis::config]
      -> Class[redis::service]
    }
  }
}

class redis::install {
  #// config files
  file { 'redis::config':
    path => "${redis::app_dir}/config",
    ensure => $redis::dir_ensure,
    force => true,
    require => File[redis::app_dir],
  }

  file { 'redis::config::env':
    path => "${redis::app_dir}/config/redis.conf",
    ensure => $redis::file_ensure,
    content => epp('redis/config/redis.conf', $redis::app_settings),
    require => File[redis::config],
  }

  #// setup service
  stdapp::service::install { redis: }
}

class redis::config {
  #//empty
}

class redis::service {
  stdapp::service { redis: }
}
