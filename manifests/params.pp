class redis::params {
  include stdapp

  $app_dir = "${stdapp::base_dir}/redis"
  $service_prefix = 'docker-compose@redis'

  $value = parseyaml(file('redis/params.yml'))
  $settings = std::nv($value['redis::settings'], Hash, {})
}
